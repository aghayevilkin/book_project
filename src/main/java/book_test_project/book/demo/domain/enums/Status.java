package book_test_project.book.demo.domain.enums;

public enum Status {
    ACTIVE,
    INACTIVE,
    DELETED
}
