package book_test_project.book.demo.domain.enums;

public enum UserAuthority {
    USER, ADMIN, SUPER_USER, ANONYMOUS, AGENT, PUBLISHER
}
