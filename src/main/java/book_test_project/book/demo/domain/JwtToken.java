package book_test_project.book.demo.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JwtToken {
    
    private String idToken;

    public JwtToken(String idToken) {
        this.idToken = idToken;
    }

    @JsonProperty("id_token")
    String getIdToken() {
        return idToken;
    }

    void setIdToken(String idToken) {
        this.idToken = idToken;
    }
}