package book_test_project.book.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Setter
@Getter
@Builder
@Table(name = "authority")
@AllArgsConstructor
@NoArgsConstructor
public class Role {

    @NotNull
    @Size(max = 100)
    @Id
    @Column(length = 100)
    private String authority;

    @OneToMany(mappedBy = "role")
    List<User> users;
}
