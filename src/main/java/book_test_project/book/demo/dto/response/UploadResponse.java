package book_test_project.book.demo.dto.response;

import lombok.Data;

@Data
public class UploadResponse {

    private Long id;
}
