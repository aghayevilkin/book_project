package book_test_project.book.demo.dto.response;

import lombok.Data;

import java.util.Set;

@Data
public class BookResponseDto {

    private Long id;

    private String name;

    private String description;

    private String title;

    private String currency;

    private Long prize;

    private Long page;

    private Set<AuthorResponseDto> authors;

    private CategoryResponseDto category;
}
