package book_test_project.book.demo.dto.response;


import lombok.Data;
@Data
public class UserAdminResponseDto {

    /**DTO for admin**/
    private Long id;
    private String username;
    private RoleResponseDto role;
}

