package book_test_project.book.demo.dto.response;

import lombok.Data;

@Data
public class UserDetailsResponseDto {

    private Long id;
    private String username;
    private String firstName;
    private String lastName;
    private Long image;
    private String email;
}
