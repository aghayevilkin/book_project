package book_test_project.book.demo.dto.response;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BookResponseForAllUserDto {

    private Long id;
    private String name;
    private Long page;
    // TODO add other fields
}
