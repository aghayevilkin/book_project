package book_test_project.book.demo.dto.response;

import lombok.Data;


@Data
public class UserResponseDto {

    /** DTO for register process **/
    private Long id;
    private String username;
}

