package book_test_project.book.demo.dto.response;
import lombok.Data;

@Data
public class RoleResponseDto {

    private String authority;
}
