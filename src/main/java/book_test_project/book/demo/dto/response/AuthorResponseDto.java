package book_test_project.book.demo.dto.response;

import lombok.Data;

@Data
public class AuthorResponseDto {

    private Long id;

    private String firstName;

    private String lastName;

    private int age;

    private Long image;
}
