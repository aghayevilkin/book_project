package book_test_project.book.demo.dto.request;

import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class AuthorRequestDto {

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @NotNull
    @Positive
    private int age;

    private Base64RequestDto imageDetails;
}
