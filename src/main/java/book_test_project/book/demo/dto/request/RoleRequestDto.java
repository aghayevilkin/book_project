package book_test_project.book.demo.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RoleRequestDto {

    @NotNull
    private String authority;
}
