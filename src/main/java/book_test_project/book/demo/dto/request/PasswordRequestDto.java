package book_test_project.book.demo.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PasswordRequestDto {

    @NotNull
    private String oldPassword;

    @NotNull
    private String newPassword;

    @NotNull
    private String repeatPassword;
}
