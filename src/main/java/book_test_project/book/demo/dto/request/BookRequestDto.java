package book_test_project.book.demo.dto.request;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Set;

@Data
public class BookRequestDto {

    @Column(name = "name", unique = true, nullable = false)
    private String name;

    @NotNull
    private String description;

    @NotNull
    private String title;

    @NotNull
    @Positive
    private Long page;

    @NotNull
    private String currency;

    @NotNull
    @Positive
    private Long prize;

    @NotNull
    private Set<Long> authors;

    @NotNull
    private Long category;

    private Base64RequestDto imageDetails;

}
