package book_test_project.book.demo.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ImageIdDto {

    @NotNull
    private Long imageID;
}
