package book_test_project.book.demo.dto.request;


import book_test_project.book.demo.domain.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserStatusRequestDto {

    @NotNull
    private Status status;
}
