package book_test_project.book.demo.dto.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserRegisterRequestDto {

    @Size(min = 5, message = "Your user name must have at least 5 characters")
    @NotBlank(message = "Please provide a user name")
    private String username;

    @Email(message = "Please provide a valid Email")
    @NotBlank(message = "Please provide your email")
    private String email;

    @Size(min = 4, message = "Your password must have at least 5 characters")
    @NotBlank(message = "Please provide your password")
    private String password;

    @Size(min = 4, message = "Your password must have at least 5 characters")
    @NotBlank(message = "Please provide your password")
    private String repeatPassword;

    @NotBlank(message = "Please provide your name")
    private String firstName;

    @NotBlank(message = "Please provide your lastname")
    private String lastName;

    private String userDescription;
}
