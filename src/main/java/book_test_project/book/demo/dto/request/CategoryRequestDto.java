package book_test_project.book.demo.dto.request;

import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Data
public class CategoryRequestDto {

    @Column(name = "name")
    private String name;

    @NotNull
    private String description;
}
