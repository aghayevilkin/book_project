package book_test_project.book.demo.dto.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PageFilterDto {

    private int size;
    private int page;
}
