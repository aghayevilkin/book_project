package book_test_project.book.demo.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class Base64RequestDto {

    @NotBlank
    private String base64;

    @NotBlank
    private String extension;
}
