package book_test_project.book.demo.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class UpdateImageRequestDto {

    @NotBlank
    private String base64;

    @NotBlank
    private String extension;

    @NotNull
    private Long imageID;
}
