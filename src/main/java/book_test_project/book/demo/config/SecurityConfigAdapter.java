package book_test_project.book.demo.config;

import book_test_project.book.demo.config.jwt.AuthFilterConfigurerAdapter;
import book_test_project.book.demo.config.jwt.AuthService;
import book_test_project.book.demo.domain.enums.UserAuthority;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

@RequiredArgsConstructor
@Configuration
@EnableWebSecurity
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfigAdapter extends WebSecurityConfigurerAdapter {

    /**
     * Authority
     **/
    private static final String ADMIN = UserAuthority.ADMIN.name();
    private static final String USER = UserAuthority.USER.name();
    private static final String SUPER_USER = UserAuthority.SUPER_USER.name();


    private final SecurityProperties securityProperties;
    private final List<AuthService> authServices;


    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable()
                .cors().configurationSource(corsConfigurationSource());
        http.authorizeRequests()
                .antMatchers(swaggerUrls).permitAll()
                .antMatchers(permittedUrls).permitAll()

                .antMatchers(belongToAdmin).hasAnyAuthority(ADMIN, SUPER_USER)
                .antMatchers(belongToUser).hasAnyAuthority(USER, ADMIN, SUPER_USER);

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.apply(new AuthFilterConfigurerAdapter(authServices));
//        super.configure(http);
        http.authorizeRequests((requests) -> requests.anyRequest().authenticated());
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication().withUser("admin").
//                password(bCryptPasswordEncoder().encode("12345")).roles(SUPER_USER);
//        auth
//                .userDetailsService(userDetailsService())
//                .passwordEncoder(bCryptPasswordEncoder());
    }

    protected CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = securityProperties.getCors();
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    private final String[] swaggerUrls = {
            "/v2/api-docs",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**"
    };
    private final String[] permittedUrls = {
            "/v1/auth/refresh",
            "/v1/auth/login",
            "/v1/user/register",
            "/v1/book/list",
            "/v1/book/search",
            "/v1/author/search",
            "/v1/category/search",
            "/v1/book/all/books"
    };
    private final String[] belongToUser = {
            "/v1/author/user/**",
            "/v1/auth/forgot-password",
            "/v1/book/user/**",
            "/v1/category/user/**",
            "/v1/files/**",
            "/v1/user/add/image/{id}",
            "/v1/user/change/password/{id}",
            "/v1/user/update/image/{id}",
            "/v1/user/delete/image/{id}",
            "/v1/user/verify/{verify-code}",
            "/v1/user/details"

    };
    private final String[] belongToAdmin = {
            "/v1/author/admin/**",
            "/v1/book/admin/**",
            "/v1/category/admin/**",
            "/v1/user/admin/**",
            "/v1/role/**"
    };
}
