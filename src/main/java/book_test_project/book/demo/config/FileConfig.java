package book_test_project.book.demo.config;

import book_test_project.book.demo.exception.InvalidStateException;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Component
public class FileConfig {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter
            .ofPattern("yyyy" + File.separator + "MM" + File.separator + "dd");

    private String getDatePath() {
        LocalDate localDate = LocalDate.now();
        return localDate.format(DATE_TIME_FORMATTER);
    }

    public String generateFilePath(String basePath, String extension) {
        return generateFilePath(basePath, extension, "");
    }

    public String generateFilePath(String basePath, @NonNull String extension, String prefix) {
        if (StringUtils.isBlank(extension)) {
            throw new InvalidStateException("File extension must be not null or empty");
        }

        String uuid = UUID.randomUUID().toString();

        return basePath
                .concat(File.separator)
                .concat(getDatePath())
                .concat(File.separator)
                .concat(StringUtils.trimToEmpty(prefix))
                .concat(uuid)
                .concat(".")
                .concat(extension);
    }

    @SneakyThrows
    public void writeByteArrayToFile(File file, byte[] bytes) {
        FileUtils.writeByteArrayToFile(file, bytes);
    }

    @SneakyThrows
    public byte[] readFileToByteArray(File file) {
        return FileUtils.readFileToByteArray(file);
    }
}
