package book_test_project.book.demo.config;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;

@Getter
@Configuration
@ConfigurationProperties(prefix = "security")
public class SecurityProperties {

    private final JwtProperties jwtProperties = new JwtProperties();

    private final CorsConfiguration cors = new CorsConfiguration();

    @Getter
    @Setter
    public static class JwtProperties {

        @NotNull
        private String secret;

        @NotNull
        private long tokenValidityInSeconds;

        @NotNull
        private long tokenValidityInSecondsForRememberMe;
    }

}
