package book_test_project.book.demo.exception;


public class NotFoundException extends RuntimeException {

    public NotFoundException(String message){
        super(message);
    }
}
