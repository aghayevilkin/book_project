package book_test_project.book.demo.exception;

public class RoleAlreadyExistException extends InvalidStateException {

    private final static long serialVersionUID = 2L;

    public RoleAlreadyExistException(String name) {
        super(String.format("Role < %s > already exist ", name));
    }
}

