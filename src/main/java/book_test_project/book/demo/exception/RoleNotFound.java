package book_test_project.book.demo.exception;

public class RoleNotFound extends NotFoundException{

    public RoleNotFound(String authorityName) {
        super(String.format("Authority Name < %s > not found ", authorityName));

    }
}
