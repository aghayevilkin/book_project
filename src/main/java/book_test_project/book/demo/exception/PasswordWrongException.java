package book_test_project.book.demo.exception;

public class PasswordWrongException extends InvalidStateException {
    public static final String OLD_PASSWORD_WRONG = "Old password is wrong";

    public PasswordWrongException() {
        super(OLD_PASSWORD_WRONG);
}}
