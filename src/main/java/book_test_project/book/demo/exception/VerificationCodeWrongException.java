package book_test_project.book.demo.exception;

public class VerificationCodeWrongException extends InvalidStateException {
    public static final String VERIFICATION_CODE_WRONG = "Verification code is wrong!";

    public VerificationCodeWrongException() {
        super(VERIFICATION_CODE_WRONG);
}}
