package book_test_project.book.demo.exception;

public class UserNotFoundId extends NotFoundException{
    private static String MESSAGE = "not found user id = %s";

    public UserNotFoundId(Long id) {
        super(String.format(MESSAGE,id));
    }
}
