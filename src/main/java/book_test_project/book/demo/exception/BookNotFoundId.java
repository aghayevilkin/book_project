package book_test_project.book.demo.exception;

public class BookNotFoundId extends NotFoundException{

    private static final String MESSAGE = "No book with ID %s";

    public BookNotFoundId(Long id){
        super(String.format(MESSAGE, id));

    }
}
