package book_test_project.book.demo.exception;

public class CategoryAlreadyExistException extends InvalidStateException {

    private final static long serialVersionUID = 2L;

    public CategoryAlreadyExistException(String name) {
        super(String.format("Category \"%s\" already exist ", name));
    }
}

