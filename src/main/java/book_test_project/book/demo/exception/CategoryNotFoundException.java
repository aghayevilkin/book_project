package book_test_project.book.demo.exception;

public class CategoryNotFoundException extends NotFoundException {

    private static String MESSAGE = "not found category id = %s";

    public CategoryNotFoundException(Long id) {
        super(String.format(MESSAGE,id));
    }

}
