package book_test_project.book.demo.exception;

public class UserNotFound extends NotFoundException{

    public UserNotFound(String user) {
        super(String.format("User < %s > not found ", user));

    }
}
