package book_test_project.book.demo.exception;

public class ImageNotFoundException extends NotFoundException{
    private static String MESSAGE = "Not found image id = %s";

    public ImageNotFoundException(Long id) {
        super(String.format(MESSAGE,id));
    }
}
