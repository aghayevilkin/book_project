package book_test_project.book.demo.exception;

public class AuthorAlreadyExistException extends InvalidStateException {

    private final static long serialVersionUID = 2L;

    public AuthorAlreadyExistException(String name) {
        super(String.format("Author \"%s\" already exist ", name));
    }
}

