package book_test_project.book.demo.exception;

public class NewPasswordNotMatch extends InvalidStateException {

    public static final String NOT_MATCH_NEW_PASSWORD = "New password is not equal with repeat password";

    public NewPasswordNotMatch() {
        super(NOT_MATCH_NEW_PASSWORD);
    }}


