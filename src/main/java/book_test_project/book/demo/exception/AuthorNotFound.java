package book_test_project.book.demo.exception;

public class AuthorNotFound extends NotFoundException{

    public AuthorNotFound(String authorName) {
        super(String.format("Author Name < %s > not found ", authorName));

    }
}
