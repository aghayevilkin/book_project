package book_test_project.book.demo.exception;

public class NotMatchVerificationCode extends InvalidStateException {
    public static final String NOT_MATCH_IMAGE_ID = "NOT match verification code";

    public NotMatchVerificationCode() {
        super(NOT_MATCH_IMAGE_ID);
}}
