package book_test_project.book.demo.exception;

public class NotMatchImageId extends InvalidStateException {
    public static final String NOT_MATCH_IMAGE_ID = "NOT match image id!";

    public NotMatchImageId() {
        super(NOT_MATCH_IMAGE_ID);
}}
