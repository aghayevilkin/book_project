package book_test_project.book.demo.exception;

public class RepeatPasswordWrongException extends InvalidStateException {
    public static final String REPEAT_PASSWORD_WRONG = "Repeat password is wrong";

    public RepeatPasswordWrongException() {
        super(REPEAT_PASSWORD_WRONG);
}}
