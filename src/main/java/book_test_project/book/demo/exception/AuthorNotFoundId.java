package book_test_project.book.demo.exception;

public class AuthorNotFoundId extends NotFoundException{
    private static String MESSAGE = "not found author id = %s";

    public AuthorNotFoundId(Long id) {
        super(String.format(MESSAGE,id));
    }
}
