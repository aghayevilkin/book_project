package book_test_project.book.demo.exception;

public class AuthenticationException extends RuntimeException {
    public AuthenticationException() {
        super("Unauthorized");
    }
}