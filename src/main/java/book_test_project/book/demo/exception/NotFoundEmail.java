package book_test_project.book.demo.exception;

public class NotFoundEmail extends InvalidStateException{

    private static final String NOT_FOUND_EMAIL = "Not found email";
    public NotFoundEmail() {
        super(NOT_FOUND_EMAIL);

    }
}
