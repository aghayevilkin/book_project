package book_test_project.book.demo.exception;

public class InvalidStateException extends RuntimeException {

    private static final long serialVersionUID = 2L;

    public InvalidStateException(String message) {
        super(message);
    }
}
