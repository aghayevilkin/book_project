package book_test_project.book.demo.exception;

public class BookNameAlreadyExistException extends InvalidStateException {

    private final static long serialVersionUID = 2L;

    public BookNameAlreadyExistException(String bookName) {
        super(String.format("Book Name < %s > already exist ", bookName));
    }
}
