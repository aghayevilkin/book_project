package book_test_project.book.demo.exception;

public class UsernameAlreadyExistException extends InvalidStateException {

    private final static long serialVersionUID = 2L;

    public UsernameAlreadyExistException(String userName) {
        super(String.format("Username \"%s\" already exist ", userName));
    }
}

