package book_test_project.book.demo.service.impl;

import book_test_project.book.demo.config.FileConfig;
import book_test_project.book.demo.domain.Image;
import book_test_project.book.demo.dto.request.Base64RequestDto;
import book_test_project.book.demo.dto.request.UpdateImageRequestDto;
import book_test_project.book.demo.dto.response.ImageResponseDto;
import book_test_project.book.demo.dto.response.UploadResponse;
import book_test_project.book.demo.exception.ImageNotFoundException;
import book_test_project.book.demo.exception.NotFoundException;
import book_test_project.book.demo.repository.ImageRepository;
import book_test_project.book.demo.service.FileService;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.codec.binary.Base64;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {

    @Value("${application.file.basePath}")
    private String dPath;


    private final ModelMapper modelMapper;
    private final ImageRepository imageRepository;
    private final FileConfig fileConfig;

    @Override
    public UploadResponse uploadImage(Base64RequestDto requestDto) {
        String filePath = createFilePath(requestDto.getExtension());
        fileConfig.writeByteArrayToFile(new java.io.File(filePath), decode(requestDto.getBase64()));
        Image image = new Image();
        image.setPath(filePath);
        imageRepository.save(image);
        return modelMapper.map(image, UploadResponse.class);
    }

    @Override
    public ImageResponseDto getImage(Long id) {
        return imageRepository.findById(id)
                .map(image -> mapToBase64ResponseDto(id, encode(image.getPath())))
                .orElseThrow(() -> new ImageNotFoundException(id));
    }

    @Override
    public UploadResponse updateImage(UpdateImageRequestDto requestDto) {
        Image image = imageRepository.findById(requestDto.
                getImageID()).orElseThrow(() -> new NotFoundException("Not found image"));
        String filePath = createFilePath(requestDto.getExtension());
        fileConfig.writeByteArrayToFile(new java.io.File(filePath), decode(requestDto.getBase64()));
        image.setPath(filePath);
        imageRepository.save(image);
        return modelMapper.map(image, UploadResponse.class);
    }

    @Override
    public void removeImage(Long imageId) {
        Image image = imageRepository.findById(imageId)
                .orElseThrow(() -> new NotFoundException("Not found image"));
        imageRepository.delete(image);
    }

    private ImageResponseDto mapToBase64ResponseDto(Long id, byte[] bytes) {
        return ImageResponseDto
                .builder()
                .id(id)
                .bytes(bytes)
                .build();
    }

    private String createFilePath(String extension) {
        return fileConfig.generateFilePath(dPath, extension);

    }

    private byte[] encode(String path) {
        return fileConfig.readFileToByteArray(new java.io.File(path));
    }

    private byte[] decode(String imageBase64) {
        return Base64.decodeBase64(imageBase64);
    }
}
