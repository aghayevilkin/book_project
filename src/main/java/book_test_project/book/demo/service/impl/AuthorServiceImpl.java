package book_test_project.book.demo.service.impl;

import book_test_project.book.demo.domain.Author;
import book_test_project.book.demo.dto.request.AuthorRequestDto;
import book_test_project.book.demo.dto.request.UpdateImageRequestDto;
import book_test_project.book.demo.dto.response.AuthorResponseDto;
import book_test_project.book.demo.dto.search.GenericSearchDto;
import book_test_project.book.demo.exception.AuthorAlreadyExistException;
import book_test_project.book.demo.exception.AuthorNotFoundId;
import book_test_project.book.demo.exception.NotMatchImageId;
import book_test_project.book.demo.repository.AuthorRepository;
import book_test_project.book.demo.search.SearchSpecification;
import book_test_project.book.demo.service.AuthorService;
import book_test_project.book.demo.web.rest.FileController;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;
    private final ModelMapper mapper;
    private final FileController fileController;


    @Override
    public AuthorResponseDto createAuthor(AuthorRequestDto requestDto) {
        authorRepository.findByFirstNameIgnoreCase(requestDto.getFirstName()).
                ifPresent(author -> {
                    throw new AuthorAlreadyExistException(requestDto.getFirstName());
                });
        Author author = mapper.map(requestDto, Author.class);
        author.setImage(fileController.upload(requestDto.getImageDetails()).getId());
        return mapper.map(authorRepository.save(author), AuthorResponseDto.class);
    }

    @Override
    public AuthorResponseDto update(Long id, AuthorRequestDto requestDto) {
        Author author = authorCheckId(id);
        mapper.map(requestDto, author);
        return mapper.map(authorRepository.save(author), AuthorResponseDto.class);
        // TODO in update method there is a problem
    }

    @Override
    public void deleteAuthor(Long id) {
        Author author = authorCheckId(id);
        fileController.removeImage(author.getImage());
        author.setImage(null);
        authorRepository.delete(author);
    }

    @Override
    public AuthorResponseDto findById(Long id) {
        return authorRepository.findById(id)
                .map((s) -> mapper.map(s, AuthorResponseDto.class))
                .orElseThrow(() -> new AuthorNotFoundId(id));
    }

    /**
     * TODO Search methods
     **/
    @Override
    public Page<AuthorResponseDto> search(GenericSearchDto genericSearchDto, Pageable pageable) {
        return authorRepository
                .findAll(new SearchSpecification<>(genericSearchDto.getCriteria()), pageable)
                .map(author -> mapper.map(author, AuthorResponseDto.class));
    }

    @Override
    public void deleteImage(Long id, Long imageId) {
        Author author = authorCheckId(id);
        if (!imageId.equals(author.getImage())){
            throw new NotMatchImageId();
        }
        fileController.removeImage(imageId);
        author.setImage(null);
        authorRepository.save(author);
    }

    @Override
    public void updateImage(Long id, UpdateImageRequestDto requestDto) {
        Author author = authorCheckId(id);
        author.setImage(fileController.update(requestDto).getId());
        authorRepository.save(author);
    }

    public Author authorCheckId(Long id) {
        return authorRepository.findById(id).orElseThrow(() -> new AuthorNotFoundId(id));
    }
}
