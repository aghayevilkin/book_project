package book_test_project.book.demo.service.impl;

import book_test_project.book.demo.domain.Category;
import book_test_project.book.demo.dto.request.CategoryRequestDto;
import book_test_project.book.demo.dto.response.CategoryResponseDto;
import book_test_project.book.demo.dto.search.GenericSearchDto;
import book_test_project.book.demo.exception.CategoryAlreadyExistException;
import book_test_project.book.demo.exception.CategoryNotFoundException;
import book_test_project.book.demo.repository.CategoryRepository;
import book_test_project.book.demo.search.SearchSpecification;
import book_test_project.book.demo.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final ModelMapper mapper;
    private final CategoryRepository categoryRepository;

    @Override
    public CategoryResponseDto createCategory(CategoryRequestDto requestDto) {

        categoryRepository.findByNameIgnoreCase(requestDto.getName())
                .ifPresent(category -> {
                    throw new CategoryAlreadyExistException(requestDto.getName());
                });

        Category category = mapper.map(requestDto, Category.class);
        return mapper.map(categoryRepository.save(category), CategoryResponseDto.class);
    }

    @Override
    public CategoryResponseDto update(Long id, CategoryRequestDto requestDto) {

        Category category = categoryRepository.findById(id).orElseThrow(() -> new CategoryNotFoundException(id));
        mapper.map(requestDto, category);
        return mapper.map(categoryRepository.save(category), CategoryResponseDto.class);
    }

    @Override
    public void deleteCategory(Long id) {
        Category category = categoryRepository.findById(id).orElseThrow(() -> new CategoryNotFoundException(id));
        categoryRepository.delete(category);
    }

    @Override
    public CategoryResponseDto findById(Long id) {
        return categoryRepository.findById(id)
                .map((s) -> mapper.map(s, CategoryResponseDto.class))
                .orElseThrow(() -> new CategoryNotFoundException(id));
    }
    @Override
    public Page<CategoryResponseDto> search(GenericSearchDto genericSearchDto, Pageable pageable) {
        return categoryRepository
                .findAll(new SearchSpecification<>(genericSearchDto.getCriteria()), pageable)
                .map(author -> mapper.map(author, CategoryResponseDto.class));
    }
}
