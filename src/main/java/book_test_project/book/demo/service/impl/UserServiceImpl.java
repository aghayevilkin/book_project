package book_test_project.book.demo.service.impl;

import book_test_project.book.demo.domain.Role;
import book_test_project.book.demo.domain.User;
import book_test_project.book.demo.domain.enums.Status;
import book_test_project.book.demo.domain.enums.UserAuthority;
import book_test_project.book.demo.dto.request.Base64RequestDto;
import book_test_project.book.demo.dto.request.UpdateImageRequestDto;
import book_test_project.book.demo.dto.request.PasswordRequestDto;
import book_test_project.book.demo.dto.request.UserAuthorityRequestDto;
import book_test_project.book.demo.dto.request.UserRegisterRequestDto;
import book_test_project.book.demo.dto.request.UserStatusRequestDto;
import book_test_project.book.demo.dto.response.UserDetailsResponseDto;
import book_test_project.book.demo.exception.AlreadyExistException;
import book_test_project.book.demo.exception.NewPasswordNotMatch;
import book_test_project.book.demo.exception.NotFoundEmail;
import book_test_project.book.demo.exception.NotMatchImageId;
import book_test_project.book.demo.exception.NotMatchVerificationCode;
import book_test_project.book.demo.exception.PasswordWrongException;
import book_test_project.book.demo.exception.RepeatPasswordWrongException;
import book_test_project.book.demo.exception.RoleNotFound;
import book_test_project.book.demo.exception.UserNotFound;
import book_test_project.book.demo.exception.UserNotFoundId;
import book_test_project.book.demo.exception.UsernameAlreadyExistException;
import book_test_project.book.demo.repository.RoleRepository;
import book_test_project.book.demo.repository.UserRepository;
import book_test_project.book.demo.service.SendEmailMessageService;
import book_test_project.book.demo.service.UserService;
import book_test_project.book.demo.service.jwt.JwtService;
import book_test_project.book.demo.web.rest.FileController;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.UUID;


@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final RoleRepository authorityRepository;
    private final ModelMapper mapper;
    private final PasswordEncoder encoder;
    private final FileController fileController;
    private final JwtService jwtService;
    private final SendEmailMessageService sendEmailMessageService;

    @Override
    public void createUser(UserRegisterRequestDto request) {

        //check username
        userRepository.findUserByUsernameIgnoreCase(request.getUsername()).ifPresent(user -> {
            throw new UsernameAlreadyExistException(request.getUsername());
        });
        User user = createUserEntityObject(request, checkAuthorities(UserAuthority.USER.name()));
        System.out.println("create");
        userRepository.save(user);
    }

    @Override
    public void updateUserStatus(Long id, UserStatusRequestDto request) {
        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundId(id));
        if (user.getStatus().equals(Status.DELETED)) {
            throw new AlreadyExistException("Not allowd because user deleted!");
        }
        if (request.getStatus().equals(Status.DELETED)) {
            user.setDeletionDate(LocalDateTime.now());
        }
        user.setStatus(request.getStatus());
        userRepository.save(user);
    }

    @Override
    public void updateUserAuthority(Long id, UserAuthorityRequestDto name) {
        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundId(id));
        if (user.getStatus().equals(Status.DELETED)) {
            throw new AlreadyExistException("Not allowd because user deleted!");
        }
        Role role = authorityRepository.findByAuthorityIgnoreCase(name.getAuthority().toUpperCase())
                .orElseThrow(() -> new RoleNotFound(name.getAuthority().toUpperCase()));
        user.setRole(role);
        userRepository.save(user);
    }

    @Override
    public void addImage(Long id, Base64RequestDto requestDto) {
        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundId(id));
        if (user.getImage() != null) {
            throw new AlreadyExistException("User image already exist!");
        }
        user.setImage(fileController.upload(requestDto).getId());
        userRepository.save(user);
    }

    @Override
    public void changePassword(Long id, PasswordRequestDto dto) {
        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundId(id));
        if (!encoder.matches(dto.getOldPassword(), user.getPassword())) {
            throw new PasswordWrongException();
        }
        if (!dto.getNewPassword().equals(dto.getRepeatPassword())) {
            throw new NewPasswordNotMatch();
        }
        // method not exactly
        //TODO change method structure for token
        user.setPassword(encoder.encode(dto.getNewPassword()));
        userRepository.save(user);
    }

    @Override
    public void updateImage(Long id, UpdateImageRequestDto requestDto) {
        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundId(id));
        user.setImage(fileController.update(requestDto).getId());
        userRepository.save(user);
    }

    @Override
    public void deleteImage(Long id, Long imageId) {
        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundId(id));
        if (!imageId.equals(user.getImage())) {
            throw new NotMatchImageId();
        }
        fileController.removeImage(imageId);
        user.setImage(null);
        userRepository.save(user);
    }

    @Override
    public UserDetailsResponseDto findUserDetails(String token) {
        String jwt = token.substring(7);
        Claims claims = jwtService.parseToken(jwt);
        return mapper.map(userRepository.findByUsername(claims.getSubject())
                .orElseThrow(() -> new UserNotFound(claims.getSubject())), UserDetailsResponseDto.class);

    }

    @Override
    public void activateAccount(String verificationCode, String email) {
        User user = userRepository.findByEmail(email).orElseThrow(NotFoundEmail::new);
        if (!verificationCode.equals(user.getVerificationCode())){
            throw new NotMatchVerificationCode();
        }
        user.setVerificationCode(null);
        user.setStatus(Status.ACTIVE);
        userRepository.save(user);
    }

    @Override
    public void forgotPassword(String email) {
        User user = userRepository.findByEmail(email).orElseThrow(NotFoundEmail::new);
        if (user.getStatus().equals(Status.INACTIVE)){
            throw new NotFoundEmail();
        }
        String random = user.getFirstName().toLowerCase()+
                UUID.randomUUID().toString().replace("-", "").substring(0, 5);
        sendEmailMessageService.sendMessage("Reset Password",
                resetPassword(random,user),user.getEmail());
        user.setPassword(encoder.encode(random));
        userRepository.save(user);
    }

    private String resetPassword(String random, User user) {
        return "Be careful! \n" +
                "Hi " + user.getFirstName() +" "+user.getLastName()+"\n"+
                "_______________________\n" +
                "Your new password: "+random;
    }

    @Override
    @Transactional
    public void deleteUser(Long id) {
        User user = userRepository.findById(id).
                orElseThrow(() -> new UserNotFoundId(id));
        if (user.getImage()!=null){
            fileController.removeImage(user.getImage());
        }
        user.setImage(null);
        user.setStatus(Status.DELETED);
        user.setDeletionDate(LocalDateTime.now());
    }

    @Override
    public UserDetailsResponseDto findByUsername(String name) {
        return userRepository.findByUsername(name).
                map((s) -> mapper.map(s, UserDetailsResponseDto.class))
                .orElseThrow(() -> new UserNotFound(name));
    }

    private User createUserEntityObject(UserRegisterRequestDto request, Role authorities) {
        User user = mapper.map(request, User.class);
        if (!request.getPassword().equals(request.getRepeatPassword())) {
            throw new RepeatPasswordWrongException();
        }
        String code = generateRandomCode();
        sendEmailMessageService.sendMessage("Verification",generateVerificationBody(code,request),
                request.getEmail());
        user.setPassword(encoder.encode(request.getPassword()));
        user.setRole(authorities);
        user.setStatus(Status.INACTIVE);
        user.setVerificationCode(code);
        return user;
    }

    private Role checkAuthorities(String authoritiesString) {
        return authorityRepository.findByAuthorityIgnoreCase(authoritiesString)
                .orElseThrow(() -> new RoleNotFound(authoritiesString));
    }
    // TODO getalluser method will must be create
    // TODO change user get method with id
    // TODO short data incoming method

    public String generateRandomCode() {
        return UUID.randomUUID().toString().replace("-", "").substring(0,6);
    }
    private String generateVerificationBody(String random,UserRegisterRequestDto request){
        return "Be careful! \n" +
                "Hi " + request.getFirstName() +" "+request.getLastName()+"\n"+
                "_______________________\n \n" +
                "Your verification code: "+random;
    }

}
