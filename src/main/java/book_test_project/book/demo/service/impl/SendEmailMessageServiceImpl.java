package book_test_project.book.demo.service.impl;

import book_test_project.book.demo.service.SendEmailMessageService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@Service
@RequiredArgsConstructor
public class SendEmailMessageServiceImpl implements SendEmailMessageService {

    private final JavaMailSender mailSender;

    @Value("${application.message.email}")
    private String email;

    @SneakyThrows
    @Override
    public void sendMessage(String title, String body, String toMail) {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper mailMessage = new MimeMessageHelper(message, true, "UTF-8");
        mailMessage.setFrom(email,"Book Store");
        mailMessage.setSubject(title);
        mailMessage.setText(body);
        mailMessage.setTo(toMail);
        mailSender.send(message);
    }

}
