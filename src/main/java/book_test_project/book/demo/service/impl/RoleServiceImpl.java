package book_test_project.book.demo.service.impl;

import book_test_project.book.demo.domain.Role;
import book_test_project.book.demo.dto.request.RoleRequestDto;
import book_test_project.book.demo.dto.response.RoleResponseDto;
import book_test_project.book.demo.exception.AlreadyExistException;
import book_test_project.book.demo.exception.RoleNotFound;
import book_test_project.book.demo.exception.RoleAlreadyExistException;
import book_test_project.book.demo.repository.RoleRepository;
import book_test_project.book.demo.service.RoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository repository;
    private final ModelMapper mapper;

    @Override
    public void createRole(RoleRequestDto requestDto) {
        Example<Role> role = Example.of(Role.builder()
                .authority(requestDto.getAuthority().toUpperCase()).build());
        if (repository.exists(role)) {
            throw new RoleAlreadyExistException(requestDto.getAuthority().toUpperCase());
        }
        repository.save(Role.builder().authority(requestDto.getAuthority().toUpperCase()).build());
    }
    @Override
    public void deleteRole(String name) {

        Role role = repository.findByAuthorityIgnoreCase(name.toUpperCase())
                .orElseThrow(() -> new RoleNotFound(name.toUpperCase()));
        if (role.getUsers() != null ){
            throw new AlreadyExistException("Belong this role user or users exist! You can`t delete this role.");
        }
        repository.delete(role);
    }

    @Override
    public List<RoleResponseDto> listAllRole() {
        return repository.findAll()
                .stream()
                .map(role -> mapper.map(role, RoleResponseDto.class))
                .collect(Collectors.toList());
    }
}
