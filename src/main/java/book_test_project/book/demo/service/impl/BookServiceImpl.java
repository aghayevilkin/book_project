package book_test_project.book.demo.service.impl;

import book_test_project.book.demo.domain.Author;
import book_test_project.book.demo.domain.Book;
import book_test_project.book.demo.domain.Category;
import book_test_project.book.demo.dto.request.BookRequestDto;
import book_test_project.book.demo.dto.request.PageFilterDto;
import book_test_project.book.demo.dto.request.UpdateImageRequestDto;
import book_test_project.book.demo.dto.response.BookResponseDto;
import book_test_project.book.demo.dto.response.BookResponseForAllUserDto;
import book_test_project.book.demo.dto.search.GenericSearchDto;
import book_test_project.book.demo.exception.AuthorNotFoundId;
import book_test_project.book.demo.exception.BookNameAlreadyExistException;
import book_test_project.book.demo.exception.BookNotFoundId;
import book_test_project.book.demo.exception.CategoryNotFoundException;
import book_test_project.book.demo.exception.NotMatchImageId;
import book_test_project.book.demo.repository.AuthorRepository;
import book_test_project.book.demo.repository.BookRepository;
import book_test_project.book.demo.repository.CategoryRepository;
import book_test_project.book.demo.search.SearchSpecification;
import book_test_project.book.demo.service.BookService;
import book_test_project.book.demo.web.rest.FileController;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final ModelMapper mapper;
    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final CategoryRepository categoryRepository;
    private final FileController fileController;


    @Override
    public BookResponseDto createBook(BookRequestDto requestDto) {

        /** book check **/
        bookRepository.findBookByNameIgnoreCase(requestDto.getName()).ifPresent(book -> {
            throw new BookNameAlreadyExistException(requestDto.getName());
        });

        /** book map **/
        Book book = mapper.map(requestDto, Book.class);
        book.setCategory(getCategory(requestDto.getCategory()));
        book.setAuthors(getAuthorList(requestDto.getAuthors()));
        book.setImage(fileController.upload(requestDto.getImageDetails()).getId());
        book.setCurrency(requestDto.getCurrency().toUpperCase());

        /** save book **/
        return mapper.map(bookRepository.save(book), BookResponseDto.class);
    }

    @Override
    public BookResponseDto update(Long id, BookRequestDto bookRequestDto) {
        Book book = bookCheckId(id);
        mapper.map(bookRequestDto, book);
        book.setCategory(getCategory(bookRequestDto.getCategory()));
        book.setAuthors(getAuthorList(bookRequestDto.getAuthors()));
        book.setCurrency(bookRequestDto.getCurrency().toUpperCase());
        return mapper.map(bookRepository.save(book), BookResponseDto.class);
    }

    @Override
    public void deleteBook(Long id) {
        Book book = bookCheckId(id);
        fileController.removeImage(book.getImage());
        book.setImage(null);
        bookRepository.delete(book);
    }

    @Override
    public BookResponseDto findById(Long id) {
        return bookRepository.findById(id).map((book) -> mapper.map(book, BookResponseDto.class)).orElseThrow(() -> new BookNotFoundId(id));
    }

    private Set<Author> getAuthorList(Set<Long> authors) {
        return authors.stream().map(id -> authorRepository.findById(id).orElseThrow(() -> new AuthorNotFoundId(id))).collect(Collectors.toSet());
    }

    private Category getCategory(Long category) {
        return categoryRepository.findById(category).orElseThrow(() -> new CategoryNotFoundException(category));
    }

    /**
     * Search methods
     **/
    @Override
    public Page<BookResponseDto> search(GenericSearchDto genericSearchDto, Pageable pageable) {
        return bookRepository.findAll(new SearchSpecification<>(genericSearchDto.getCriteria()), pageable).map(book -> mapper.map(book, BookResponseDto.class));
    }

    @Override
    public List<BookResponseDto> listAllBooks() {
        return bookRepository.findAll().stream().map(book -> mapper.map(book, BookResponseDto.class)).collect(Collectors.toList());
    }

    @Override
    public void deleteImage(Long id, Long imageId) {
        Book book = bookCheckId(id);
        if (!imageId.equals(book.getImage())) {
            throw new NotMatchImageId();
        }
        fileController.removeImage(imageId);
        book.setImage(null);
        bookRepository.save(book);
    }

    @Override
    public void updateImage(Long id, UpdateImageRequestDto requestDto) {
        Book book = bookCheckId(id);
        book.setImage(fileController.update(requestDto).getId());
        bookRepository.save(book);
    }

    @Override
    public Page<BookResponseForAllUserDto> getBooks(PageFilterDto pageFilter) {
        Pageable pageable = PageRequest.of(pageFilter.getPage(), pageFilter.getSize());
        return bookRepository.findAll(pageable).map(book -> mapper.map(book, BookResponseForAllUserDto.class));
    }

    public Book bookCheckId(Long id) {
        return bookRepository.findById(id).orElseThrow(() -> new BookNotFoundId(id));
    }
}
