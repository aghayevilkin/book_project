package book_test_project.book.demo.service;

import book_test_project.book.demo.dto.request.RoleRequestDto;
import book_test_project.book.demo.dto.response.RoleResponseDto;

import java.util.List;


public interface RoleService {
    void createRole(RoleRequestDto requestDto);

    void deleteRole(String name);

    List<RoleResponseDto> listAllRole();
}