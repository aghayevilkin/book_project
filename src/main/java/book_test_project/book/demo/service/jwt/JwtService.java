package book_test_project.book.demo.service.jwt;

import book_test_project.book.demo.config.SecurityProperties;
import book_test_project.book.demo.domain.enums.Status;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public final class JwtService {

    private final SecurityProperties applicationProperties;

    private Key key;

    @PostConstruct
    private void init() {
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode(applicationProperties.getJwtProperties().getSecret());
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public Claims parseToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public String issueToken(Authentication authentication, Duration duration, Status status) {
        log.trace("Issue JWT token to {} for {}", authentication.getPrincipal(), duration);

        Set<String> authorities = authentication.getAuthorities().stream()
                .map(Object::toString)
                .collect(Collectors.toSet());

        final JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(authentication.getName())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(duration)))
                .signWith(key, SignatureAlgorithm.HS512)
                .claim("rule", authorities);

        return jwtBuilder.compact();
    }

    public String refresh(String token, Duration duration) {
        final Date createdDate = new Date();
        final Claims claims = parseToken(token);
        claims.setIssuedAt(createdDate);
        claims.setExpiration(Date.from(Instant.now().plus(duration)));
        final JwtBuilder jwtBuilder = Jwts.builder()
                .setClaims(claims)
                .signWith(key, SignatureAlgorithm.HS512);
        return jwtBuilder.compact();
    }

    public String logout(String token) {
        return null;
        // empty for now!
    }
}
