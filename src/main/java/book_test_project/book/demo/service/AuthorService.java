package book_test_project.book.demo.service;

import book_test_project.book.demo.dto.request.AuthorRequestDto;
import book_test_project.book.demo.dto.request.Base64RequestDto;
import book_test_project.book.demo.dto.request.ImageIdDto;
import book_test_project.book.demo.dto.request.UpdateImageRequestDto;
import book_test_project.book.demo.dto.response.AuthorResponseDto;
import book_test_project.book.demo.dto.search.GenericSearchDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface AuthorService {
    AuthorResponseDto createAuthor(AuthorRequestDto requestDto);

    AuthorResponseDto update(Long id, AuthorRequestDto requestDto);

    void deleteAuthor(Long id);

    AuthorResponseDto findById(Long id);

    Page<AuthorResponseDto> search(GenericSearchDto dto, Pageable pageable);

    void deleteImage(Long id, Long imageId);

    void updateImage(Long id, UpdateImageRequestDto requestDto);
}
