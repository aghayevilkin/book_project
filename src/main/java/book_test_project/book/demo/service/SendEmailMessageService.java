package book_test_project.book.demo.service;

public interface SendEmailMessageService {
    void sendMessage(String title, String body, String toMail);
}
