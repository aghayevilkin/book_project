package book_test_project.book.demo.service;


import book_test_project.book.demo.dto.request.CategoryRequestDto;
import book_test_project.book.demo.dto.response.CategoryResponseDto;
import book_test_project.book.demo.dto.search.GenericSearchDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CategoryService {
    CategoryResponseDto createCategory(CategoryRequestDto requestDto);

    CategoryResponseDto update(Long id, CategoryRequestDto requestDto);

    void deleteCategory(Long id);

    CategoryResponseDto findById(Long id);

    Page<CategoryResponseDto> search(GenericSearchDto dto, Pageable pageable);

}
