package book_test_project.book.demo.service;

import book_test_project.book.demo.dto.request.BookRequestDto;
import book_test_project.book.demo.dto.response.BookResponseForAllUserDto;
import book_test_project.book.demo.dto.request.PageFilterDto;
import book_test_project.book.demo.dto.request.UpdateImageRequestDto;
import book_test_project.book.demo.dto.response.BookResponseDto;
import book_test_project.book.demo.dto.search.GenericSearchDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BookService {
    BookResponseDto createBook(BookRequestDto requestDto);

    BookResponseDto update(Long id, BookRequestDto bookRequestDto);

    void deleteBook(Long id);

    BookResponseDto findById(Long id);

    Page<BookResponseDto> search(GenericSearchDto dto, Pageable pageable);

    List<BookResponseDto> listAllBooks();

    void deleteImage(Long id, Long imageId);

    void updateImage(Long id, UpdateImageRequestDto requestDto);

    Page<BookResponseForAllUserDto> getBooks(PageFilterDto pageFilter);
}
