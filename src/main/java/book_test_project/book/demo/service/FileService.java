package book_test_project.book.demo.service;

import book_test_project.book.demo.dto.request.Base64RequestDto;
import book_test_project.book.demo.dto.request.ImageIdDto;
import book_test_project.book.demo.dto.request.UpdateImageRequestDto;
import book_test_project.book.demo.dto.response.ImageResponseDto;
import book_test_project.book.demo.dto.response.UploadResponse;

public interface FileService {

    UploadResponse uploadImage(Base64RequestDto requestDto);

    ImageResponseDto getImage(Long id);

    UploadResponse updateImage(UpdateImageRequestDto requestDto);

    void removeImage(Long imageId);
}
