package book_test_project.book.demo.service;

import book_test_project.book.demo.dto.request.Base64RequestDto;
import book_test_project.book.demo.dto.request.UpdateImageRequestDto;
import book_test_project.book.demo.dto.request.PasswordRequestDto;
import book_test_project.book.demo.dto.request.UserAuthorityRequestDto;
import book_test_project.book.demo.dto.request.UserRegisterRequestDto;
import book_test_project.book.demo.dto.request.UserStatusRequestDto;
import book_test_project.book.demo.dto.response.UserDetailsResponseDto;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;


@Service
public interface UserService {
    void createUser(UserRegisterRequestDto request) throws MessagingException;

    void deleteUser(Long id);

    UserDetailsResponseDto findByUsername(String name);

    void updateUserStatus(Long id , UserStatusRequestDto mUserDto);
    void updateUserAuthority(Long id , UserAuthorityRequestDto mUserDto);

    void addImage(Long id, Base64RequestDto requestDto);

    void changePassword(Long id, PasswordRequestDto dto);

    void updateImage(Long id, UpdateImageRequestDto requestDto);

    void deleteImage(Long id, Long imageId);

    UserDetailsResponseDto findUserDetails(String token);

    void activateAccount(String code, String email);

    void forgotPassword(String email);
}
