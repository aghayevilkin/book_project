package book_test_project.book.demo.web.rest;

import book_test_project.book.demo.dto.request.Base64RequestDto;
import book_test_project.book.demo.dto.request.UpdateImageRequestDto;
import book_test_project.book.demo.dto.response.ImageResponseDto;
import book_test_project.book.demo.dto.response.UploadResponse;
import book_test_project.book.demo.service.FileService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;


@RestController
@RequestMapping("/v1/files")
@RequiredArgsConstructor
public class FileController {

    private final FileService fileService;

    @PostMapping("/image")
    public UploadResponse upload(@RequestBody Base64RequestDto requestDto) {
        return fileService.uploadImage(requestDto);
    }
    @PostMapping("/change/image")
    public UploadResponse update(@RequestBody UpdateImageRequestDto requestDto) {
        return fileService.updateImage(requestDto);
    }
    @GetMapping("/image/{id}")
    public ImageResponseDto getImage(@PathVariable Long id) {
        return fileService.getImage(id);
    }
    @DeleteMapping("/image/remove")
    public void removeImage(@RequestBody @NotNull Long imageId){
        fileService.removeImage(imageId);
    }
}
