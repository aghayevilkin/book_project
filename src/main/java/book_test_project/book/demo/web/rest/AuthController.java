package book_test_project.book.demo.web.rest;

import book_test_project.book.demo.domain.JwtToken;
import book_test_project.book.demo.domain.enums.Status;
import book_test_project.book.demo.dto.jwt.LoginDto;
import book_test_project.book.demo.exception.AuthenticationException;
import book_test_project.book.demo.repository.UserRepository;
import book_test_project.book.demo.service.UserService;
import book_test_project.book.demo.service.jwt.JwtService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Duration;

@Slf4j
@RestController
@RequestMapping("/v1/auth")
@RequiredArgsConstructor
public class AuthController {

    private static final Duration FIFTEN_MINUTE = Duration.ofMinutes(15);
    private static final Duration ONE_DAY = Duration.ofDays(1);
    private static final Duration ONE_WEEK = Duration.ofDays(7);


    private final JwtService jwtService;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final UserRepository userRepository;
    private final UserService userService;

    @PostMapping("/login")
    public ResponseEntity<JwtToken> authorize(@Valid @RequestBody LoginDto loginDto) {
        log.trace("Authenticating user {}", loginDto.getUsername());

        userRepository.findByUsername(loginDto.getUsername()).ifPresent(user -> {
            if (!user.getStatus().equals(Status.ACTIVE) || user.getStatus() == null) {
                throw new AuthenticationException();
            }
        });

        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword());

        log.info("My authenticationToken token {}", authenticationToken);
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);

        String jwt = jwtService.issueToken(authentication, ONE_DAY, Status.ACTIVE);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + jwt);
        return new ResponseEntity<>(new JwtToken(jwt), httpHeaders, HttpStatus.OK);
    }

    public void logout(String authorization) {
        // TODO empty for now!
    }

    @PostMapping("/refresh")
    public ResponseEntity<JwtToken> refreshToken(String token) {
        String jwt = jwtService.refresh(token.substring(7), FIFTEN_MINUTE);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + jwt);
        return new ResponseEntity<>(new JwtToken(jwt), httpHeaders, HttpStatus.OK);
    }
    @PostMapping("/forgot-password")
    public void forgotPasswordResponse(@RequestParam @NotNull String email) {
        userService.forgotPassword(email);
    }
    /**
     * Object to return JWT as Authentication body.
     */
    private Duration getDuration(Boolean rememberMe) {
        return ((rememberMe != null) && rememberMe) ? ONE_WEEK : ONE_DAY;
    }
}
