package book_test_project.book.demo.web.rest;

import book_test_project.book.demo.dto.request.Base64RequestDto;
import book_test_project.book.demo.dto.request.PasswordRequestDto;
import book_test_project.book.demo.dto.request.UpdateImageRequestDto;
import book_test_project.book.demo.dto.request.UserAuthorityRequestDto;
import book_test_project.book.demo.dto.request.UserRegisterRequestDto;
import book_test_project.book.demo.dto.request.UserStatusRequestDto;
import book_test_project.book.demo.dto.response.UserDetailsResponseDto;
import book_test_project.book.demo.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/v1/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/admin/{name}")
    public UserDetailsResponseDto getUser(@PathVariable("name") String name) {
        return userService.findByUsername(name);
    }
    @PostMapping("/details")
    public UserDetailsResponseDto getUserDetails(@RequestBody String token) {
        return userService.findUserDetails(token);
    }

    @SneakyThrows
    @PostMapping("/register")
    public void createUser(@RequestBody @Valid UserRegisterRequestDto mrUserDto) {
        userService.createUser(mrUserDto);
    }

    @PutMapping("/admin/status/{id}")
    public void updateUserStatus(@Valid @PathVariable(value = "id") Long id, UserStatusRequestDto mUserDto) {
        userService.updateUserStatus(id, mUserDto);
    }
    @PutMapping("/change/password/{id}")
    public void changePassword(@Valid @PathVariable(value = "id") Long id, PasswordRequestDto dto) {
        userService.changePassword(id, dto);
    }
    @PutMapping("/admin/role/{id}")
    public void updateUserAuthority(@Valid @PathVariable(value = "id") Long id, UserAuthorityRequestDto mUserDto) {
        userService.updateUserAuthority(id, mUserDto);
    }

    @PostMapping("/add/image/{id}")
    public void addImage(@Valid @PathVariable(value = "id") Long id, @RequestBody Base64RequestDto requestDto) {
        userService.addImage(id, requestDto);
    }
    @PutMapping("/update/image/{id}")
    public void updateImage(@Valid @PathVariable(value = "id") Long id, @RequestBody UpdateImageRequestDto requestDto) {
        userService.updateImage(id, requestDto);
    }
    @DeleteMapping("/delete/image/{id}")
    public void deleteImage(@PathVariable("id") Long id, Long imageId) {
        userService.deleteImage(id,imageId);
    }
    @DeleteMapping("/admin/{id}")
    public void deleteUser(@PathVariable("id") Long id) {
        userService.deleteUser(id);
    }
    @PutMapping("/verify/{verify-code}")
    public void verifyEmail(@PathVariable(value = "verify-code")String code,@NotNull @RequestParam String email){
        userService.activateAccount(code,email);
    }
}
