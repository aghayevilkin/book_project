package book_test_project.book.demo.web.rest;

import book_test_project.book.demo.dto.request.AuthorRequestDto;
import book_test_project.book.demo.dto.request.UpdateImageRequestDto;
import book_test_project.book.demo.dto.response.AuthorResponseDto;
import book_test_project.book.demo.dto.search.GenericSearchDto;
import book_test_project.book.demo.service.AuthorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/v1/author")
@RequiredArgsConstructor
public class AuthorController {

    private final AuthorService authorService;

    @PostMapping("/admin")
    @ResponseStatus(HttpStatus.CREATED)
    public AuthorResponseDto createAuthor(@RequestBody @Valid AuthorRequestDto requestDto) {
        return authorService.createAuthor(requestDto);
    }
    @DeleteMapping("/user/image/{id}")
    public void deleteImage(@PathVariable("id") Long id, Long imageId) {
        authorService.deleteImage(id, imageId);
    }

    @PutMapping("/user/update/image/{id}")
    public void updateImage(@Valid @PathVariable(value = "id") Long id, @RequestBody UpdateImageRequestDto requestDto) {
        authorService.updateImage(id, requestDto);
    }

    @PostMapping("/search")
    public Page<AuthorResponseDto> searchAuthor(@RequestBody GenericSearchDto dto, Pageable pageable) {
        log.trace("Search author request");
        return authorService.search(dto, pageable);
    }

    @PutMapping("/admin/{id}")
    @ResponseStatus(HttpStatus.OK)
    public AuthorResponseDto update(@PathVariable("id") Long id,
                                    @RequestBody @Valid AuthorRequestDto requestDto) {
        return authorService.update(id, requestDto);
    }

    @DeleteMapping("/admin/{id}")
    public void deleteAuthor(@PathVariable("id") Long id) {
        authorService.deleteAuthor(id);
    }

    @GetMapping("/user/{id}")
    public AuthorResponseDto getAuthor(@PathVariable("id") Long id) {
        return authorService.findById(id);
    }

}
