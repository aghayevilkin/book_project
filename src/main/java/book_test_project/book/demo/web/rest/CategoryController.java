package book_test_project.book.demo.web.rest;

import book_test_project.book.demo.dto.request.CategoryRequestDto;
import book_test_project.book.demo.dto.response.CategoryResponseDto;
import book_test_project.book.demo.dto.search.GenericSearchDto;
import book_test_project.book.demo.service.CategoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/v1/category")
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;

    @PostMapping("/admin")
    public CategoryResponseDto createCategory(@RequestBody @Valid CategoryRequestDto requestDto) {
        return categoryService.createCategory(requestDto);
    }
    @PostMapping("/search")
    public Page<CategoryResponseDto> searchCategory(@RequestBody GenericSearchDto dto, Pageable pageable) {
        log.trace("Search category request");
        return categoryService.search(dto, pageable);
    }

    @PutMapping("/admin/{id}")
    public CategoryResponseDto update(@PathVariable("id") Long id,
                                  @RequestBody @Valid CategoryRequestDto requestDto) {
        return categoryService.update(id, requestDto);
    }

    @DeleteMapping("/admin/{id}")
    public void deleteCategory(@PathVariable("id") Long id) {
        categoryService.deleteCategory(id);
    }

    @GetMapping("/user/{id}")
    public CategoryResponseDto getCategory(@PathVariable("id") Long id) {
        return categoryService.findById(id);
    }

}
