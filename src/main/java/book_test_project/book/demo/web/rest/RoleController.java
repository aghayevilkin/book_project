package book_test_project.book.demo.web.rest;

import book_test_project.book.demo.dto.request.RoleRequestDto;
import book_test_project.book.demo.dto.response.RoleResponseDto;
import book_test_project.book.demo.service.RoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/v1/role")
@RequiredArgsConstructor
public class RoleController {

    private final RoleService service;

    @PostMapping("/admin")
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@Valid RoleRequestDto requestDto) {
         service.createRole(requestDto);
    }

    @DeleteMapping("/admin/{name}")
    public void deleteAuthor(@PathVariable("name") String name) {
        service.deleteRole(name);
    }
    @GetMapping("/admin/list")
    public List<RoleResponseDto> listAllRole() {
        return service.listAllRole();
    }

}
