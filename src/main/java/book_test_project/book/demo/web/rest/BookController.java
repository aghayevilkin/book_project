package book_test_project.book.demo.web.rest;

import book_test_project.book.demo.dto.request.BookRequestDto;
import book_test_project.book.demo.dto.response.BookResponseForAllUserDto;
import book_test_project.book.demo.dto.request.PageFilterDto;
import book_test_project.book.demo.dto.request.UpdateImageRequestDto;
import book_test_project.book.demo.dto.response.BookResponseDto;
import book_test_project.book.demo.dto.search.GenericSearchDto;
import book_test_project.book.demo.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/v1/book")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @PostMapping("/admin")
    public BookResponseDto createBook(@RequestBody @Valid BookRequestDto requestDto) {
        return bookService.createBook(requestDto);
    }
    @PostMapping("/search")
    public Page<BookResponseDto> searchBook(@RequestBody GenericSearchDto dto, Pageable pageable) {
        log.trace("Search book request");
        return bookService.search(dto, pageable);
    }
    @PostMapping("/all/books")
    public Page<BookResponseForAllUserDto> getBooksForAllUser(PageFilterDto pageFilter){
        return bookService.getBooks(pageFilter);
    }
    @PutMapping("/admin/{id}")
    public BookResponseDto update(@PathVariable("id") Long id,
                                  @RequestBody @Valid BookRequestDto requestDto) {
        return bookService.update(id, requestDto);
    }
    @DeleteMapping("/user/image/{id}")
    public void deleteImage(@PathVariable("id") Long id, Long imageId) {
        bookService.deleteImage(id, imageId);
    }

    @PutMapping("/user/update/image/{id}")
    public void updateImage(@Valid @PathVariable(value = "id") Long id, @RequestBody UpdateImageRequestDto requestDto) {
        bookService.updateImage(id, requestDto);
    }

    @DeleteMapping("/admin/{id}")
    public void deleteBook(@PathVariable("id") Long id) {
        bookService.deleteBook(id);
    }

    @GetMapping("/user/{id}")
    public BookResponseDto getBook(@PathVariable("id") Long id) {
        return bookService.findById(id);
    }

    @GetMapping("/list")
    public List<BookResponseDto> listAllBooks() {
        return bookService.listAllBooks();
    }
}
