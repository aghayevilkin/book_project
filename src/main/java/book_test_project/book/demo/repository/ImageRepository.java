package book_test_project.book.demo.repository;

import book_test_project.book.demo.domain.Image;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ImageRepository extends JpaRepository<Image, Long> {
}
