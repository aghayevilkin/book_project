package book_test_project.book.demo.repository;

import book_test_project.book.demo.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface AuthorRepository extends JpaRepository<Author,Long>,
        JpaSpecificationExecutor<Author> {
    
    Optional<Author> findByFirstNameIgnoreCase(String name);

}
