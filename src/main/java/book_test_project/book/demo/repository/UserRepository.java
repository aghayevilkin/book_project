package book_test_project.book.demo.repository;



import book_test_project.book.demo.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long>,
        JpaSpecificationExecutor<User> {
    Optional<User> findUserByUsernameIgnoreCase(String username);

    Optional<User> findByUsername(String username);
    Optional<User> findByEmail(String email);
}
